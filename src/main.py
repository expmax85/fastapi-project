from fastapi import FastAPI

from src.db import init_db
from src.routes import dishes

app = FastAPI()

app.include_router(dishes.router, prefix="/api/v1")


@app.on_event("startup")
async def start_up():
    await init_db()
