from fastapi import APIRouter, Depends, HTTPException, status

from src.db import ComponentAction, DishAction, get_component_service, get_dish_service
from src.models import schemas

router = APIRouter(tags=["Dishes"])


@router.post(
    "/dish", response_model=schemas.DishDetail, status_code=status.HTTP_201_CREATED
)
async def create_dish(
    data: schemas.DishCreate, dish_service: DishAction = Depends(get_dish_service)
):
    new_dish = await dish_service.create(data=data)
    return new_dish


@router.get("/dish", response_model=list[schemas.DishesDB])
async def get_all_dishes(
    skip: int = 0,
    limit: int = 100,
    dish_service: DishAction = Depends(get_dish_service),
):
    return await dish_service.get_all(skip=skip, limit=limit)


@router.get("/dish/{dish_id}", response_model=schemas.DishDetail)
async def get_dish_by_id(
    dish_id: int, dish_service: DishAction = Depends(get_dish_service)
):
    if not (dish := await dish_service.get(dish_id=dish_id, view=True)):
        raise HTTPException(
            detail="dish not found", status_code=status.HTTP_404_NOT_FOUND
        )
    return dish


@router.patch("/dish/{dish_id}", response_model=schemas.UpdatedDish)
async def update_dish(
    dish_id: int,
    data: schemas.DishUpdate,
    dish_service: DishAction = Depends(get_dish_service),
):
    if not (upd_dish := await dish_service.update(dish_id=dish_id, data=data)):
        raise HTTPException(
            detail="dish not found", status_code=status.HTTP_404_NOT_FOUND
        )
    return upd_dish


@router.delete("/dish/{dish_id}", response_model=schemas.RemoveSuccess)
async def remove_dish(
    dish_id: int, dish_service: DishAction = Depends(get_dish_service)
):
    if not (remove := await dish_service.remove(dish_id=dish_id)):
        raise HTTPException(
            detail="dish not found", status_code=status.HTTP_404_NOT_FOUND
        )
    return {"status": remove, "message": "The dish was successfully remove"}


@router.delete(
    "/dish/{dish_id}/components/{component_id}", response_model=schemas.RemoveSuccess
)
async def remove_component_from_dish(
    dish_id: int,
    component_id: int,
    component_service: ComponentAction = Depends(get_component_service),
):
    if not (
        remove := await component_service.remove_component(
            dish_id=dish_id, component_id=component_id
        )
    ):
        raise HTTPException(
            detail="dish or component in this dish not found",
            status_code=status.HTTP_404_NOT_FOUND,
        )
    return {
        "status": remove,
        "message": "The component was successfully remove from dish",
    }
