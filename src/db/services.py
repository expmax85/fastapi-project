from collections.abc import Sequence

from fastapi import Depends
from sqlalchemy import desc, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from src.db.database import get_session
from src.models import Component, Dish, schemas


class DishAction:
    def __init__(self, session: AsyncSession):
        self.session = session

    async def create(self, data: schemas.DishCreate) -> Dish:
        db_obj = Dish(
            title=data.title, description=data.description, time_done=data.time_done
        )
        components = await self._get_or_create_components(data.components)
        self.session.add(db_obj)
        db_obj.components.extend(components)
        await self.session.commit()
        return db_obj

    async def _get_or_create_components(self, list_elems: list[str]) -> list[Component]:
        result = list()
        for elem in list_elems:
            title = elem.lower().capitalize()
            stmt = await self.session.execute(
                select(Component).where(Component.title == title)
            )
            component = stmt.scalars().first()
            if not component:
                component = await self._create_component(title)
            result.append(component)
        return result

    async def _create_component(self, title: str) -> Component:
        component = Component(title=title)
        self.session.add(component)
        await self.session.commit()
        return component

    async def update(self, dish_id: int, data: schemas.DishUpdate) -> Dish | None:
        components = list()
        if updated_dish := await self.get(dish_id=dish_id):
            for key, value in data.dict(exclude_unset=True).items():
                if key != "components":
                    setattr(updated_dish, key, value)
                elif key == "components":
                    components = await self._update_components(
                        value, getattr(updated_dish, key)
                    )
            self.session.add(updated_dish)
            if components:
                updated_dish.components.extend(components)
            await self.session.commit()
        return updated_dish

    async def _update_components(
        self, update_list: list[str], obj_list: list[Component]
    ) -> list[Component]:
        result = list()
        obj_titles = [str(elem).lower().capitalize() for elem in obj_list]
        update_titles = [elem.lower().capitalize() for elem in update_list]
        if set_new := set.difference(set(update_titles), set(obj_titles)):
            result = await self._get_or_create_components(list(set_new))
        return result

    async def remove(self, dish_id: int) -> bool:
        if result := await self.get(dish_id=dish_id):
            await self.session.delete(result)
            await self.session.commit()
        return True

    async def get_all(self, skip: int = 0, limit: int = 100) -> Sequence[Dish]:
        result = await self.session.execute(
            select(Dish)
            .order_by(desc(Dish.views))
            .order_by(Dish.time_done)
            .offset(skip)
            .limit(limit)
        )
        return result.scalars().all()

    async def get(self, dish_id: int, view: bool = False) -> Dish | None:
        dish = await self.session.execute(
            select(Dish)
            .options(selectinload(Dish.components))
            .where(Dish.id == dish_id)
        )
        result = dish.scalars().first()
        if result and view:
            result.views += 1
            self.session.add(result)
            await self.session.commit()
        return result


class ComponentAction:
    def __init__(self, session: AsyncSession):
        self.session = session

    async def get(self, component_id: int) -> Component | None:
        elem = await self.session.execute(
            select(Component).where(Component.id == component_id)
        )
        component = elem.scalars().first()
        return component

    async def remove_component(self, dish_id: int, component_id: int) -> bool:
        result = await self.session.execute(
            select(Dish)
            .options(selectinload(Dish.components))
            .where(Dish.id == dish_id)
        )
        dish = result.scalars().first()
        if not dish:
            return False
        component = await self.get(component_id)
        if component not in dish.components:
            return False
        else:
            dish.components.remove(component)
            await self.session.commit()
            return True


def get_component_service(session: AsyncSession = Depends(get_session)):
    return ComponentAction(session=session)


def get_dish_service(session: AsyncSession = Depends(get_session)):
    return DishAction(session=session)
