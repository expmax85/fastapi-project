from typing import AsyncGenerator

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine

from src.config import settings
from src.models import Base

engine = create_async_engine(settings.DATABASE_URL)
async_session = AsyncSession(bind=engine, expire_on_commit=False)


async def get_session() -> AsyncGenerator:
    async with async_session as session:
        yield session


async def init_db():
    async with engine.connect() as conn:
        await conn.run_sync(Base.metadata.create_all)
