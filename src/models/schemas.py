from pydantic import BaseModel, Field


class DishBase(BaseModel):
    title: str
    time_done: int = Field(..., gt=1)
    description: str
    components: list[str] = Field(..., min_items=1)


class DishCreate(DishBase):
    ...


class DishUpdate(DishBase):
    title: str = Field(None)
    time_done: int = Field(None, gt=1)
    description: str = Field(None)
    components: list[str] = Field(None, min_items=1)


class Component(BaseModel):
    id: int
    title: str

    class Config:
        orm_mode = True


class DishDB(DishBase):
    id: int

    class Config:
        orm_mode = True


class DishinDB(BaseModel):
    id: int
    title: str
    time_done: int

    class Config:
        orm_mode = True


class DishesDB(DishinDB):
    views: int = 0


class DishDetail(DishinDB):
    description: str
    components: list[Component]


class UpdatedDish(DishDetail):
    ...


class RemoveSuccess(BaseModel):
    status: bool
    message: str
