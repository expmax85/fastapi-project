from sqlalchemy import Column, ForeignKey, Integer, String, Table
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship


class Base(DeclarativeBase):
    pass


dish_components = Table(
    "dish_components",
    Base.metadata,
    Column("dish_id", ForeignKey("dishes.id", ondelete="CASCADE"), primary_key=True),
    Column(
        "component_id",
        ForeignKey("components.id", ondelete="CASCADE"),
        primary_key=True,
    ),
)


class Dish(Base):
    __tablename__ = "dishes"

    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str] = mapped_column(String(30))
    time_done: Mapped[int]
    views: Mapped[int] = mapped_column(Integer, default=0)
    description: Mapped[str] = mapped_column(String(100))

    components: Mapped[list["Component"]] = relationship(
        secondary=dish_components, overlaps="components", passive_deletes=True
    )


class Component(Base):
    __tablename__ = "components"

    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str] = mapped_column(String(30))

    def __repr__(self):
        return str(self.title)
