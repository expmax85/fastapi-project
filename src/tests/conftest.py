import asyncio
from collections.abc import Generator

import pytest
import pytest_asyncio
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine

from src.config import settings
from src.db.database import get_session
from src.main import app
from src.models import Base, Component, Dish


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


async def db_session():
    engine = create_async_engine(settings.TEST_DATABASE_URL)
    async with engine.connect() as connection:
        trans = await connection.begin()
        session = AsyncSession(connection, expire_on_commit=False)
        await connection.run_sync(Base.metadata.create_all)

        async def _fixture(_session):
            dishes = [
                Dish(
                    title="test dish 1", description="test description 1", time_done=10
                ),
                Dish(
                    title="test dish 2",
                    description="test description 2",
                    time_done=15,
                    views=2,
                ),
            ]
            components = [Component(title="elem 1"), Component(title="elem 2")]
            dishes[0].components.extend(components)
            dishes[1].components.extend(components)
            _session.add_all(dishes)
            await _session.commit()

        await _fixture(session)
        try:
            yield session
        finally:
            await session.close()
            await trans.rollback()
            await connection.close()


app.dependency_overrides[get_session] = db_session


@pytest_asyncio.fixture
async def test_app():
    async with AsyncClient(app=app, base_url="http://test.io") as client:
        yield client
