import pytest


@pytest.mark.asyncio
async def test_create_dish(test_app):
    response = await test_app.post(
        "api/v1/dish",
        json={
            "title": "Test dish",
            "time_done": 20,
            "description": "test description",
            "components": ["test component"],
        },
    )
    assert response.status_code == 201, response.text
    data = response.json()
    assert data["title"] == "Test dish"
    assert "id" in data
    assert len(data["components"]) == 1


@pytest.mark.asyncio
async def test_get_dish(test_app):
    response = await test_app.get("/api/v1/dish/1")

    assert response.status_code == 200, response.text
    data = response.json()
    assert data["title"] == "test dish 1"
    assert data["time_done"] == 10
    assert data["id"] == 1
    assert len(data["components"]) == 2


@pytest.mark.asyncio
async def test_get_dishes(test_app):
    response = await test_app.get("/api/v1/dish")
    assert response.status_code == 200, response.text
    data = response.json()
    assert len(data) == 2
    assert data[0]["views"] == 2
    assert data[1]["views"] == 0
